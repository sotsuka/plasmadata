contract Evaluation {

  uint[] public entries;
  uint[] public entryRound;
  address[] public submitters;
  uint public round;
  uint counter;
  uint maxEntries;
  mapping(uint => uint) uniqueEntries;
  mapping(address => uint) oracleLastEntryRound;
  address[] reviewees; //oracles to be evaluated
  mapping(uint => uint) voteCounter;

  uint[] public scores;


  constructor(address[] subjects){
    counter = 0;
    maxEntries = 3;
    round = 1;
    reviewees = subjects;
  }

  function add(uint data) public returns (bool) {
    if (oracleLastEntryRound[msg.sender] != round){
      entries.push(data);
      entryRound.push(round);
      submitters.push(msg.sender);
      voteCounter[data] = voteCounter[data] + 1;

      oracleLastEntryRound[msg.sender] = round;
      counter += 1;
      if (counter >= maxEntries){
        scores.push(scoreConsensus());
        round += 1;
        counter = 0;
      }
    }
    return true;
  }

  function scoreConsensus() internal returns (uint) {
    uint startIndex = (round - 1) * maxEntries;
    uint maxVotes = 0;
    uint score;
    for (uint i = 0; i < maxEntries; i++) {
      uint foundScore = entries[startIndex + i];
      uint cand = checkVoteCounter(foundScore);
      if (maxVotes < cand){
        maxVotes = cand;
        score = foundScore;
      }
      delete voteCounter[foundScore];
    }
    return score;
  }

  function incScore(uint score) internal {
    voteCounter[score]++;
  }

  function resetScore(uint score) public returns(bool) {
    voteCounter[score] = 0;
    return true;
  }

  function checkEntries() public view returns (uint[]) {
    return entries;
  }

  function checkRounds() public view returns (uint[]) {
    return entryRound;
  }

  function checkSubmitters() public view returns (address[]) {
    return submitters;
  }

  function checkReviewees() public view returns (address[]) {
    return reviewees;
  }

  function checkScores() public view returns (uint[]) {
    return scores;
  }
  
  function checkVoteCounter(uint i) public view returns(uint) {
    return voteCounter[i];
  }

  function checkRevieweeOnTrial() public view returns(address) {
    return reviewees[round - 1];
  }
}