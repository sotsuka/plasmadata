/*
# Instructions

0. install dependencies 
  npm install
1. deploy contract
  solar deploy ./Contest.sol
2. change QtumRPC to the one you are using (line 21)
3. change fake_oracles to address you own (line 51)
4. if the fake_oracles don't have any funds, send them some coins
5. run script
  node index.js
*/
// assume: node 8 or above

const ora = require("ora")
const parseArgs = require("minimist")
const BigNumber = require('bignumber.js')

const {
  QtumRPC,
  Contract,
} = require("qtumjs")

const repo = require("./solar.json")

const rpc = new QtumRPC("http://qtumx:qtumxx@localhost:12500")
const Contest = new Contract(rpc, repo.contracts["Contest.sol"])

async function call_checkEntries() {
  const res = await Contest.call("checkEntries", [])
  return res["outputs"][0].map(x => x.toNumber())
}

async function call_checkRounds() {
  const res = await Contest.call("checkRounds", [])
  return res["outputs"][0].map(x => x.toNumber())
}

async function call_checkSubmitters() {
  const res = await Contest.call("checkSubmitters", [])
  return res["outputs"][0]
}

async function call_round() {
  const res = await Contest.call("round", [])
  return res["outputs"][0].toNumber()

}

async function ledger_to_csv() {
  const entries = await call_checkEntries()
  const rounds = await call_checkRounds()
  const submitters = await call_checkSubmitters()
  console.log(['"Entry"', '"Round"', '"Address"'].join(", "))
  for (var i = 0; i < entries.length; i++){
    console.log([entries[i], rounds[i], submitters[i]].join(", "))
  }
}

async function submit(address, ans) {
  const opt = { senderAddress: address, gasLimit: 1200000 }
  const tx = await Contest.send("add", [ans], opt)
  const confirmation = tx.confirm(1)
  ora.promise(confirmation, "confirm submission")
  await confirmation
}

async function get_last_round() {
  const round = await call_round()
  console.log(round)
}

async function simulated_submit() {
  const fake_oracles = ['QcLnMVBAU6HLtddv2HwufpzBbgrnno9Tbj', 'Qco3FvfV8FdWVoCb4DGnGDhHxiUAYjYuqK', 
                       'QfSiJx6Uci9NjPaGY8dfx1SykTL3FbB6KX', 'QTkn2k3gGDjTvHzCsB4tNPpgP9MbGSm7Td']
  const random_oracle = fake_oracles[Math.floor(Math.random() * 4)]
  const opt = { senderAddress: random_oracle }
  const simulated_answer = Math.floor(Math.random() * 100) + 1
  const tx = await Contest.send("add", [simulated_answer], opt)

  console.log("add tx:", tx.txid)
  //console.log(tx)

  // await: tx.confirm(1)
  const confirmation = tx.confirm(1)
  ora.promise(confirmation, "confirm submission")
  await confirmation
}

async function main() {
  const argv = parseArgs(process.argv.slice(2))
  const cmd = argv._[0]
  switch (cmd) {
    case "status":
      await ledger_to_csv()
      break
    case "submit":
      const owner = argv._[1]
      const ans = argv._[2]
      if (!owner || !ans) {
        throw new Error("please specify an address and an answer")
      }
      await submit(owner, ans)
      break
    case "round":
      await get_last_round()
      break
    default:
      console.log("unrecognized command", cmd)
  }
}

main().catch(err => {
  console.log("error", err)
})
