# About #

Plasma Data is a decentralized oracle specification/framework

### What is this repository for? ###

This repository provides a reference implementation of a Plasma Data style decentralized oracle

### How do I get set up? ###

0. install dependencies 

      ```bash
      npm install
      ```

1. deploy contract

     ```bash
     solar deploy Contest.sol
     ```

2. change QtumRPC to the one you are using (line 21, contest.js)

3. Start Jupyter notebook and run any of the notebooks provided

4. At the end of a contest you will be given instructions to run the evaluation dapp

5. Compile it as instructed

     ```bash
     solar deploy Evaluation.sol '[<list of oracles>]'
     ```
