contract Contest {
  uint[] entries;
  uint[] entryRound;
  address[] submitters;
  uint public round;
  uint counter;
  uint maxEntries;
  mapping(uint => uint) uniqueEntries;
  mapping(address => uint) oracleLastEntryRound;

  constructor(){
    counter = 0;
    maxEntries = 3;
    round = 1;
  }

  function add(uint data) public returns (bool) {
    if (oracleLastEntryRound[msg.sender] != round){
      entries.push(data);
      entryRound.push(round);
      submitters.push(msg.sender);

      oracleLastEntryRound[msg.sender] = round;
      counter += 1;
      if (counter >= maxEntries){
        round += 1;
        counter = 0;
      }
    }
  }
  function checkEntries() public view returns (uint[]) {
    return entries;
  }

  function checkRounds() public view returns (uint[]) {
    return entryRound;
  }

  function checkSubmitters() public view returns (address[]) {
    return submitters;
  }
}
